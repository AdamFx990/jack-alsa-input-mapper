#!/bin/sh

alsa_extract_desc() {
    printf "$1" \
        | awk -F [ '/,/{print $2}' \
        | awk -F ] '{print $1}' \
        | uniq
}

alsa_extract_name() {
    printf "$1" \
        | awk -F \: '/,/{print $2}' \
        | awk '{print $1}' \
        | uniq
}

alsa_devices_init_input() {
    IFS=$'\n'
    INPUTS=( $(arecord -l | grep card) )
    printf "Input devices:\n%s\n\n" "${INPUTS[*]}"
    
    for INPUT in ${INPUTS[@]}; do
        printf "Current input: %s\n" "$INPUT"
        jack_add_alsa_input "$INPUT"
    done
    
    unset IFS
}

alsa_devices_init_output() {
    IFS=$'\n'
    OUTPUTS=( $(aplay -l | grep card) )
    printf "Output devices:\n%s\n\n" "${OUTPUTS[*]}"

    for OUTPUT in ${OUTPUTS[@]}; do
        printf "Current output: %s\n" "$OUTPUT"
        jack_add_alsa_output "$OUTPUT"
    done
    
    unset IFS
}

jack_add_alsa_input() {
    JACK_NAME="$(alsa_extract_desc ${1}) IN"
    HW_ID="$(alsa_extract_name ${1})"
    
    alsa_in -j "$JACK_NAME" \
            -d hw:$HW_ID \
            -q $RESAMPLER_QUALITY \
                2>&1 1> /dev/null &
}

jack_add_alsa_output() {
    JACK_NAME="$(alsa_extract_desc ${1}) OUT"
    HW_ID="$(alsa_extract_name ${1})"

    alsa_out -j "$JACK_NAME" \
             -d hw:$HW_ID \
             -q $RESAMPLER_QUALITY \
                2>&1 1> /dev/null &
}

RESAMPLER_QUALITY=3

alsa_devices_init_output
alsa_devices_init_input
