#!/bin/sh

input_blue_yeti() {
    alsa_in -j "Blue Yeti Microphone" \
            -d hw:Microphone \
            -q 1 2>&1 1> /dev/null &
}

output_blue_yeti() {
    alsa_out -j "Blue Yeti Headphones" \
            -d hw:Microphone \
            -q 1 2>&1 1> /dev/null &
}

input_blue_yeti &
output_blue_yeti &
